'use strict';

const gateway = require('surgio/build/gateway');

module.exports = gateway.createHttpServer();